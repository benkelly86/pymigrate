# pyMigrate


*Environment requirements:*

1) sudo apt install python3-venv
2) Clone or move files into a directory of your choice and cd into that directory
3) python3 -m venv venv
4) source venv/bin/activate
5) pip3 install -r requirements.txt


--------------------------------------------------------

*Database:*

The database is a MySQL database with two tables:

![Screenshot](images/show_tables.png)

![Screenshot](images/des_employee.png)

![Screenshot](images/des_version.png)


employeeSalary has three existing records (version 1).

![Screenshot](images/Select_employee.png)

versionTable has one record:

![Screenshot](images/Select_version.png)


--------------------------------------------------------


*Migration script:*

You run the migration script with the arguments in this exact order:

python3 scriptname.py migrations-directory DB_User DB_Host DB_Name DB_password

For example:

python3 pymigrate.py migrations testuser localhost testdb ReallySecurePa55!


--------------------------------------------------------

*Example*

The migrations directory has a number of new versions with gaps and zeros in the file name.

![Screenshot](images/migrations.png)

Run the script.

You will be asked to check and verify that the arguments are correct.

![Screenshot](images/Step1.png)

Versions will now be checked, comparing the files in the migrations directory and the DB version taken from the versionTable. The migrations files for execution will then be presented. You will again be asked to confirm to proceed.

![Screenshot](images/Step2.png)

Records have successfully been inserted into the employeeSalary table and the version table has been updated to match the latest version in migrations.

![Screenshot](images/Step3.png)

Here's the employeeSalary table after the script completed the insert.

![Screenshot](images/employee_updated.png)

Here's the versionTable table now with the version matching the latest migrations version number.

![Screenshot](images/version_updated.png)

Running the script again will show that the versions match and there's nothing to execute.

![Screenshot](images/version_match.png)


--------------------------------------------------------

*Docker*

If prefered, you can build an image from the Dockerfile included.

docker build -t pymigrate .

![Screenshot](images/dockerrun.png)


You'll need to make sure the container can access the database and the required database permissions are in place.
