FROM python:3.8-slim-buster

ENV VIRTUAL_ENV=/opt/venv 

RUN python3 -m venv ${VIRTUAL_ENV} 

ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install dependencies:
COPY requirements.txt .
RUN pip3 install -r requirements.txt

# Create migrations directory and copy migrations files into it
RUN mkdir migrations
COPY migrations migrations

# Run the application:
COPY pymigrate.py .
CMD ["python", "pymigrate.py", "migrations", "testuser", "172.17.0.1", "testdb", "ReallySecurePas55!"]

