import mysql.connector
import sys
import os
import re


# Collect and validate arguments
def cmdargs():

  args = sys.argv

  dbdetails = {
    "migrations": args[1],
    "dbuser": args[2],
    "dbhost": args[3],
    "dbname": args[4],
    "dbpass": args[5]
  }

  print("SQL Directory: " + dbdetails["migrations"])
  print("Database User: " + dbdetails["dbuser"])
  print("Database Host: " + dbdetails["dbhost"])
  print("Database Name: " + dbdetails["dbname"])
  print("Database Password: " + dbdetails["dbpass"])

  proceed = input("Are the details above correct? (y/n): ")
  
  if proceed.lower() == "y":
    print("Great! Let's proceed.")
    print("Checking DB versions...")
    checkversion(dbdetails)
  else:
    print("OK, bye.")
    exit(0)


# Check and compare migration and DB versions
def checkversion(dbdetails):
  print("Collecting version details...")
  # Read migrations directory for version number

  try:
    files = os.listdir(dbdetails["migrations"])

    # Update list item if filename starts with 0 so that sort will order correctly
    for item in files:
      if item.startswith("0"):
        files[0] = item[1:]
      else:
        pass

    file_names = sorted(files)
    migrations_version = ""

    for item in file_names:
        v = re.findall('^[0-9]+', item)
        migrations_version = v

  except FileNotFoundError as e:
    print(e)

  # Query versionTable for version number
  try:
    connection = mysql.connector.connect(host=dbdetails["dbhost"],
                                        database=dbdetails["dbname"],
                                        user=dbdetails["dbuser"],
                                        password=dbdetails["dbpass"])

    query = "SELECT version FROM versionTable"

    cursor = connection.cursor()
    cursor.execute(query)

    dbversion = ""

    for row in cursor:
      dbversion = row

    dbversion = dbversion[0]

    print("")
    print("Migrations version: ", migrations_version[-1])
    print("DB version: ", dbversion)
    print("")
    
    cursor.close()

  except mysql.connector.Error as error:
    print("Error: {}".format(error))

  except UnboundLocalError as error:
    print("Error: {}".format(error))

  finally:
    if connection.is_connected():
      connection.close()
      print("MySQL connection is closed")

  # If the version numbers are the same, do nothing.
  # If migrations version number is higher than DB version then call migrationFiles()
  diff = []

  if int(dbversion) == int(migrations_version[-1]):
    print("Versions match, nothing to do.")
  elif int(migrations_version[-1]) > int(dbversion):
    print("There are migrations required.")
    # Work out the difference between the versions and store in list
    for i in range(int(dbversion)+1, int(migrations_version[-1])+1):
      diff.append(i)
    migrationFiles(diff, file_names, dbdetails, migrations_version)
  else:
    print("Database version is ahead of migrations version, this needs to be looked at.")
    exit(0)

# Pull together an ordered list of migration files
def migrationFiles(diff, file_names, dbdetails, migrations_version):
  
  print("Collating migration files...")
  
  # Collect all migration files that need to be inserted into the DB
  migration_files = []

  for file in file_names:
    for num in diff:
      if file.startswith(str(num)):
        migration_files.append(file)

  migration_files = sorted(migration_files)

  # Pass migration files to executeSQL()
  executeSQL(migration_files, dbdetails, migrations_version)


# Execute each migration file
def executeSQL(migration_files, dbdetails, migrations_version):

  # Add the "0" back in at the start of a file where it was removed earlier so that sort/order worked
  for index, item in enumerate(migration_files):
    if item not in os.listdir("migrations/"):
      migration_files[index] = "0" + item
  
  print("")
  print("===========================")
  print(migration_files)
  print("===========================")
  print("")

  proceed = input("Are you sure you want to proceed with executing the SQL files above? (y/n): ")

  if proceed.lower() == "y":
    print("Executing SQL.")
  else:
    print("OK, bye.")
    exit(0)

  try:
    connection = mysql.connector.connect(host=dbdetails["dbhost"],
                                        database=dbdetails["dbname"],
                                        user=dbdetails["dbuser"],
                                        password=dbdetails["dbpass"])

    cursor = connection.cursor()

    for file in migration_files:
      for line in open("migrations/" + file):
        cursor.execute(line)

    connection.commit()
    print("Records inserted successfully into table.")

    print("Updating the versionTable with the new version number.")
    cursor.execute(f"INSERT INTO versionTable (version) VALUES ({migrations_version[-1]})")
    connection.commit()
    cursor.close()

  except mysql.connector.Error as error:
    print("Failed to insert records {}".format(error))

  finally:
    if connection.is_connected():
      connection.close()
      print("MySQL connection is closed")

# Script entry point
if __name__ == "__main__":
  if len(sys.argv) < 6:
    print("""Please remember commandline arguments.
      In this order -> <migrations-directory> <DB User> <DB Host> <DB Name> <DB Password>""")
    sys.exit(1)  # aborting because of error
  else:
    cmdargs()    